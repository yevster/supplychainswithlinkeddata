# Documenting The Software Supply Chain with Linked Data #

This repository contains the sample files for the presentation.

### License ###
All `.rdf` files are licensed under the [Creative Commons Attribution Sharealike 3.0 License](https://creativecommons.org/licenses/by-sa/3.0/us/). `The_Beatles.rdf` provided by [dbpedia.org](https://dbpedia.org) under same license. "The Beatles" is a registered trademark of Apple Corps Ltd.

All other files are licensed under the [Creative Commons Attribution NonCommercial NoDerivatives 3.0 Unported License](https://creativecommons.org/licenses/by-nc-nd/3.0/).

### Contents ###

[Documenting the Software Supply Chain with Linked Data](https://bitbucket.org/yevster/supplychainswithlinkeddata/src/HEAD/Documenting%20the%20Software%20Supply%20Chain%20with%20Linked%20Data.pdf) - The Presentation.

`The_Beatles.rdf` - Massive set of RDF data in RDF/XML form, where `http://dbpedia.org/resource/The_Beatles` is the subject or the object of every triple. Used to illustrate linked data and RDF concepts.

`emptyPackage.rdf` - The result of creating a new project with [SPDX-edit](http://github.com/yevster/spdx-edit), where a single empty package is created to represent the [AppBOMination](http://github.com/yevster/App-BOM-ination) project.

`PackageRelationship.rdf` - The result of adding another package to `emptyPackage.rdf`. This new package represents the [Apache Commons Lang](https://commons.apache.org/proper/commons-lang) 3.4 package. The AppBOMination package has a static link relationship to this new package.

`appBOMination.rdf` - A further refinement of `PackageRelationship.rdf` which adds package file contents. One of the files in this package has its own proprietary license, which has been included into the document.

### Tools for SPDX (used in the presentation)###

[Twinkle](http://www.ldodds.com/projects/twinkle/) - a GUI for SPARQL queries

[SPDX Edit](https://www.github.com/yevster/spdx-edit/releases) - To create simple SPDX by hand

[SPDX Tools](https://github.com/spdx/tools/releases) - Conversion, comparison, verification, etc


[Java 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html) required to run all of the above.